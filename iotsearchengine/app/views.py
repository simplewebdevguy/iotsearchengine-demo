# views.py
import shodan
import sys
from django.shortcuts import render

# SHODAN API KEY
SHODAN_API_KEY = "61TvA2dNwxNxmWziZxKzR5aO9tFD00Nj"

# API REFERENCE
api = shodan.Shodan(SHODAN_API_KEY)

# LIST OF ITEMS WE WANT STATISTICS FOR IN OUR COUNTRY (MY IN GUYANA) KNOWN AS FACETS
# A LIST OF FACETS CAN WE VIEWED HERE https://developer.shodan.io/api
FACETS = [
    'city',
    'os',
    'org',
    'device',
    'port'
]

def app(request):
    if request.method == 'GET':
        try:
            # WE DEFINE DICTIONARIES TO HOLD THE RESULTS RETURNED FROM SHODAN
            result_dict = {}
            facet_items_dict = {}
            # WE SPECIFY THE COUNTRY WE WANT THE STATISTICS FOR.ITS' "GY" IN MY CASE
            # ALL COUNTRY CODES CAN BE FOUND HERE: https://github.com/postmodern/shodan-ruby/blob/master/lib/shodan/countries.rb
            query = 'country:GY'

            # WE CALL THE COUNT METHOD FROM API AND PASS IN THE QUERY AND THE FACETS WE NEED
            result = api.count(query, facets=FACETS)

            print('Shodan Summary Information')
            print('Query: %s' % query)
            print('Total Results: %s\n' % result['total'])

            # PRINT THE SUMMARY INFO FROM THE FACETS
            for facet in result['facets']:
                facet_title = facet
                facet_items_dict = {}

                for term in result['facets'][facet]:
                    # WE DEFINE A VARIABLE TO HOLD THE FACET AND ITS RESPECTIVE ITEM SEPARATED BY A COLON
                    facet_item = facet+':'+str(term['value'])
                    # WE THEN ADD THE TERM TO THE FACET ITEMS DICTIONARY
                    facet_items_dict.update({facet_item: term['count']})
                    # WHICH IS THEN ADDED TO THE RESULTS DICTIONARY
                    result_dict.update({facet_title: facet_items_dict})

            # THEN WE PRINT THE RESULTS
            print(result_dict)

            # THE EXCEPTION BELOW REPORTS ANY ERRORS FOUND.
        except Exception as e:
            print('Error: %s' % e)
            sys.exit(1)

    if request.method == 'POST':
        # WE DEFINE A DICTIONARY TO HOLD THE SEARCH RESULTS
        search_results = {}

        # WE EXTRACT THE QUERY FROM THE POST REQUEST
        query = request.POST.get('search-query')

        try:
            # WE PERFORM A SEARCH
            results = api.search(query)

            print('Results found: {}'.format(results['total']))
            # WE LOOP THROUGH THE RESULTS
            for result in results['matches']:
                ip = result['ip_str']
                data = result['data']
                # AND ADD EACH RESULT TO THE SEARCH RESULTS DICTIONARY
                search_results.update({ip: data})

            # THEN WE PRINT THE RESULTS
            print(search_results)

        # THE EXCEPTION BELOW REPORTS ANY ERRORS FOUND.    
        except shodan.APIError:
            print('Error')
        return render(request, 'app/results.html', {'search_results': search_results, 'query': query})

    return render(request, 'app/app.html', {'result_dict': result_dict})
    # Temp - Used to view results.html page
    # return render(request, 'app/results.html')
